function parseFile(){
	let txtFld  = document.querySelector('.txt_fld1').value;
	let file 	= document.querySelector('.file');
	if(file.files.length == 0){
		console.log("please select a file");
		return false;	
	}else{
		console.log(file.files[0])
		let fileNme  = file.files[0].name;
		let fileSize = file.files[0].size
		httpClientRequest(file.files[0], txtFld);
	}

 function httpClientRequest(file, inputTxt){
 	var formData = new FormData();
 	 formData.append("file",file);
 	 formData.append("inputTxt",inputTxt);

 	 var request = new XMLHttpRequest();
 	 request.onreadystatechange = function() {
		 if (this.readyState == 4 && this.status == 200) {
		    console.log("file uploaded successfully")
		 }
	 };
 	 request.open("POST", "processFile.php");
	 request.send(formData);
 }
	
}