<?php
class WriteTextFile{
	
	public function execute(){
		$path  = "data/sample.txt";
		$error = "unable to read file";
		$file  = fopen($path,"a") or die($error);
		$text  = "Hello this is a new line inserted Programatically \n";
		fwrite($file, $text);
		fclose($file);

		return "successfully wrote in text file\n";
	}
}
?>