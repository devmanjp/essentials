<?php
class ReadTextFile{
	
	public function execute(){
		$path  = "data/sample.txt";
		$error = "unable to read file";
		$file  = fopen($path,"r") or die($error);
		$getAllLines = $this->iterateAllLines($file);
		fclose($file);

		return $getAllLines;
	}

	private function iterateAllLines($file){
		$str = "";
		while(fgets($file) != false){
			$line = fgets($file);
			$str .= $line."\n";
		}
		return $str;
	}
}
?>